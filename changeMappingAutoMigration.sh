#!/usr/bin/env bash

INDEX_NAME=$1

elasticdump \
  --input=http://localhost:9200/"$INDEX_NAME" \
  --output="$INDEX_NAME".json \
  --type=data && \
pm2 stop all && \
curl -XDELETE http://localhost:9200/"$INDEX_NAME" && \
sleep 5 && \
pm2 start all && \
elasticdump \
  --input="$INDEX_NAME".json \
  --output=http://localhost:9200/"$INDEX_NAME" \
  --type=data