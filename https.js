const https = require('https');
const fs = require('fs');

var options = {
  hostname: 'elastic.mti.branderstudio.com',
  port: 443,
  path: '/',
  method: 'GET',
  //rejectUnauthorized: false, //можно отключить проверку доверенности сертификата
  key: fs.readFileSync('/home/tomfun/prj/tls/secure/client.key'),//мы свои, и ключ у нас есть
  passphrase: 'client',//и пасфраза
  cert: fs.readFileSync('/home/tomfun/prj/tls/secure/client.pem'),//и сертификат, который не жалко показывать
  ca: fs.readFileSync('/home/tomfun/prj/tls/secure/ca.crt')//и доверяем мы именно нашему серверу
};

https.get(options, (res) => {
  console.log('statusCode: ', res.statusCode);
  console.log('headers: ', res.headers);

  res.on('data', (d) => {
    process.stdout.write(String(d).substr(0, 100));
    process.stdout.write("\n");
  });

}).on('error', (e) => {
  console.error(e);
});