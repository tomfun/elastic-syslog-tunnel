const tls = require('tls');
const fs = require('fs');

const options = {
  //hostname: 'elastic.mti.branderstudio.com',
  servername: 'elastic.mti.branderstudio.com',
  host: 'elastic.mti.branderstudio.com',
  port: 1337,
  key: fs.readFileSync('/home/tomfun/prj/tls/secure/client.key'),//мы свои, и ключ у нас есть
  passphrase: 'client',//и пасфраза
  cert: fs.readFileSync('/home/tomfun/prj/tls/secure/client.pem'),//и сертификат, который не жалко показывать
  ca: fs.readFileSync('/home/tomfun/prj/tls/secure/ca.crt')//и доверяем мы именно нашему серверу
};

var socket = tls.connect(options, () => {
  console.log('client connected',
    socket.authorized ? 'authorized' : 'unauthorized');
  process.stdin.pipe(socket);
  process.stdin.resume();
});
socket.setEncoding('utf8');
socket.on('data', (data) => {
  console.log(data);
});
//socket.on('end', () => {
//  server.close();
//});
