## Why me
Simple **secure** log collector. Just recieve data and send it to elastic or ...   
Parse logs, metrics and other data pretty easer via **JS**,
proccessing data must be on server side (monitoring server), 
for this reason client just send data, and server chose save, process, decline.

## Requirements

### Client sdie
Any what can send data via tls.  
Ofcourse keys (private, public, certificate).  **[how to generate keys](https://gist.github.com/tomfun/1ad3d2f873625ae865b4a82e0fc9f9bc)**
Simple usecase and **instructions**: [nginx](/docs/nginx.md) (>1.7.1), [syslog](/docs/syslog.md) (3.5>).

### Server side
 - Elasticsearch
 - latest nodejs
 - pm2
 - maybe git
 - maybe grafana

## Liks
 - https://github.com/taskrabbit/elasticsearch-dump
 - https://www.balabit.com/sites/default/files/documents/syslog-ng-ose-latest-guides/en/syslog-ng-ose-guide-admin/html/procedure-configuring-mutual-tls-client.html
 - http://nginx.org/ru/docs/http/ngx_http_log_module.html
 - https://tools.ietf.org/html/rfc5424#page-7
 - https://tools.ietf.org/html/rfc3164#section-4.1.1
 
## Grafana Dashboard

### Links 

#### NginX

##### Basics
 - [nginx logs basics example](https://snapshot.raintank.io/dashboard/snapshot/svEiQ8W1oE66E1iZ2VIrDQ1PyG8npZ2p)
 - [nginx logs basics example](/dashboards/NginX-Logs-Base.json)

##### Detailed
 - [nginx logs detailed example](https://snapshot.raintank.io/dashboard/snapshot/EYz25WLcVdPaBoWY7PTU5OcYaOXhSE7Q)
 - [nginx logs detailed json](/dashboards/NginX-Logs-Detailed.json)
 
#### Syslog

 - [syslog system metrics example](/dashboards/Meta-System-Logs_screenshot.png)
 - [syslog system metrics json](/dashboards/Meta-System-Logs.json)