'use strict';

const _ = require('lodash');
const moment = require('moment');

const client = require('./elasticClient').client;

const IN_PROGRESS_WARN = +(process.env.IN_PROGRESS_WARN || 1000);

const buffer = {
  inProgress: 0,
  wait:       []
};

const mappings = {};
const currentMappings = {};

client.ping();

let progress = Promise.resolve();

function checkMappings() {
  //if (_.isEqual(mappings, currentMappings)) {
  //  return;
  //}
  let newIndexes = _.difference(Object.keys(mappings), Object.keys(currentMappings));
  if (!newIndexes.length) {
    return;
  }
  return client.indices
    .getMapping({index: newIndexes, ignoreUnavailable: true})
    .then((data) => {
      _.each(data, (o, indexName) => currentMappings[indexName] = o.mappings);
      return _.difference(Object.keys(mappings), Object.keys(currentMappings));
    })
    .then((newMappings) =>
      Promise.all(_.map(newMappings, (indexName) =>
        client.indices.create({
          index: indexName,
          // type:  'log',
          body:  {
            mappings: mappings[indexName]
          }
        })
      ))
    );
}

function worker() {
  if (buffer.inProgress > IN_PROGRESS_WARN) {
    console.warn(`${moment().format()}: elastic under pressure!, inserted ${buffer.inProgress}`);
  }
  const due = buffer.wait;
  buffer.inProgress = due.length;
  if (buffer.inProgress <= 0) {
    return;
  }
  buffer.wait = [];
  return client.bulk({body: due});
}

function startProgress() {
  if (buffer.inProgress) {
    return;
  }
  if (!buffer.wait.length) {
    return;
  }
  progress = progress
    .then(worker)
    .then(() => buffer.inProgress = 0)
    .catch((e) => {
      console.error(`${moment().format()} while processing ${buffer.inProgress} items we get error ${e}`);
      buffer.inProgress = 0;
    })
    .then(startProgress);
  buffer.inProgress = buffer.wait.length;
}

module.exports = function (data, index, type, indexMappings) {
  let indexName = index || `log_${moment().format('YYYY_MM')}`;
  let typeName = type || `log`;
  if (indexMappings) {
    mappings[indexName] = indexMappings;
    progress = progress.then(checkMappings);
  }
  buffer.wait.push({index: {_index: indexName, _type: typeName}});
  buffer.wait.push(data);
  startProgress();
};

module.exports.getIterationPromise = () => {
  //console.log('inProgress:', buffer.inProgress);
  //console.log('wait:', buffer.wait.length);
  return progress;
};

setInterval(() => {
  if (buffer.inProgress) {
    console.log(`At this moment: ${moment().format()} we have ${buffer.inProgress} items in process`);
  }
}, 60000);
