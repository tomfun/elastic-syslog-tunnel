'use strict';

const moment = require('moment');
const fs = require('fs');
const querystring = require('querystring');
const _ = require('lodash');
const helper = require('./helper');
const elastic = require('./elastic');

const LOG_VERBOSITY = process.env.INCOME_LOG_VERBOSITY || 'short';

const nginxAccessFormat = '$remote_addr  $remote_user $msec $request_method "$request_filename" "$server_protocol" $status $body_bytes_sent "$http_referer" "$http_user_agent" "$server_name" $request_time $connection $connection_requests "$request_uri" "$args" "$uri"';
//const nginxAccessFormat1 = 'req,error_code="$ercode" remote_addr="$remote_addr",remote_user="$remote_user",time_local="$time_local",request="$request",http_referer="$http_referer",request_time="$request_time",http_user_agent="$http_user_agent",http_x_forwarded_for="$http_x_forwarded_for",query="$query_string",error_code="$ercode",msisdn="$arg_msisdn",uri="$uri"';
const nginxAccesLogParser = helper.simpleExtractor(nginxAccessFormat);
//const nginxAccesLogParser1 = helper.simpleExtractor(nginxAccessFormat1);
const nginxAccesLogMappings = require('./mappings.json');

const failedPasswordFormat = 'Failed password for $user from $from port $port ssh2';
const failedPasswordParser = helper.simpleExtractor(failedPasswordFormat);
const failedPasswordFormat2 = 'Failed password for invalid user $user from $from port $port ssh2';
const failedPasswordParser2 = helper.simpleExtractor(failedPasswordFormat2);

function parseNginx(msg) {
  let parsedData = nginxAccesLogParser(msg);// || nginxAccesLogParser1(msg.MSG);
  if (!parsedData) {
    return false
  }

  let data = parsedData;
  const args = parsedData.args || parsedData.query_string;
  if (args) {
    parsedData.query = querystring.parse(args);
  }
  return data;
}

function parseSequencedData(msg) {
  let parsedData = msg.match(/^sequenceId="(\d+)"]\s(.*)$/);
  if (!parsedData) {
    return false;
  }
  const out = {
    sequenceId: parseInt(parsedData[1], 10),
    MSG: parsedData[2]
  };
  const fp = failedPasswordParser(out.MSG) || failedPasswordParser2(out.MSG);
  if (fp) {
    delete out.MSG;
    out.type = 'failedPassword';
    _.extend(out, fp);
    return out;
  }
  const pamUnixSession = out.MSG.match(/pam_unix\((.+?)\): session (\w+) for user ([A-Za-z\-]+)/);
  if (pamUnixSession) {
    delete out.MSG;
    out.type = 'pamUnixSession';
    out.pam_unix = pamUnixSession[1];
    out.sessionState = pamUnixSession[2];
    out.user = pamUnixSession[3];
    return out;
  }
  const cronCommand = out.MSG.match(/\(([A-Za-z\-]+)\) CMD \((.+)\)$/);
  if (cronCommand) {
    delete out.MSG;
    out.type = 'cronCommand';
    out.command = cronCommand[2];
    out.user = cronCommand[1];
    return out;
  }
  const ignoringMaxRetries = out.MSG.match(/PAM service\((\w+)\) ignoring max retries; (\d+) > (\d+)/);
  if (ignoringMaxRetries) {
    delete out.MSG;
    out.type = 'ignoringMaxRetries';
    out.service = ignoringMaxRetries[1];
    out.maxAuthTries = parseInt(ignoringMaxRetries[2], 10);
    out.maxAuthTriesPam = parseInt(ignoringMaxRetries[3], 10);
    return out;
  }
  return out;
}

module.exports = function (data, subject, address) {
  let s = data.toString();
  if (s.match(/^\d+ $/)) {
    return;//данные вроде не дробятся, а первая цифра - размер последующих данных
  }
  s = helper.parseSyslogData(s);
  let parsedData;
  if (parsedData = parseNginx(s.MSG)) {
    s.data = parsedData;
  } else if (parsedData = parseSequencedData(s.MSG)) {
    s.data = parsedData;
  }
  const organization = subject.O.toLowerCase().replace(/\W/g, '_').replace(/_{2,}/g, '_').replace(/^_+|_+$/g, '');
  const indexName = `log-${organization}-${moment().format('YYYY.MM.DD')}`;
  s.connectionInfo = {subject, address};
  elastic(s, indexName, null, nginxAccesLogMappings);
  switch (LOG_VERBOSITY) {
    case 'short':
      console.log(s.DATETIME, s.TAG);
      break;
    case 'full':
      console.log(subject.CN, address.address, s);
      break;
  }
};
