'use strict';

const _ = require('lodash');

/** @link https://tools.ietf.org/html/rfc5424#page-7 */
/** @link https://tools.ietf.org/html/rfc3164#section-4.1.1 */

function parseSyslogData(a) {
  let s = a.match(/^<(\d+)>(\d+) ([0-9T\-:+]+) (\S+) (\S+) (\S+) (\S+) (\S+) (.+)/);
  if (!s) {
    throw new Error('Wrong syslog format');
  }
  return {
    FACILITY: s[1] >>> 3,
    SEVERITY: s[1] % 8,
    PRIVAL:   s[1] >>> 0,
    V:        s[2],
    DATETIME: s[3],
    HOSTNAME: s[4],
    "TAG":    s[5],
    "MSG":    s[9]
  };

}
function parseNetworkSyslog(a) {
  let s = a.match(/^<(\d+)>(\w+\s\d+\s\d+:\d+:\d+)\s([^\s]+)\s([^\s]+):\s(.*)\s?/);
  throw new Error("Not implemented");
  if (!s) {
    throw new Error('Wrong network format');
  }
  return {

  };
}


function simpleExtractor(format) {
  const empty = '-';
  const keys = format.match(/\$\w+/g);
  let resultReg = format;
  resultReg = resultReg.replace(/(\.|\+|\[|\]|\{|\}|\?|\*|\(|\))/g, '\\$1');
  _.each(keys, (key, keyIndex) => {
    //console.log(`${key}`)
    let r = `(^|.)\\${key}([^\w_]|$)`;
    if (format.match(new RegExp(r, 'g')).length > 1) {
      console.trace(`Wrong format. Duplicates exists, key: ${key}`);
    }
    let s = format.match(r);
    if (s[1].length && s[2].length && s[1] !== s[2]) {
      throw new Error(`Wrong format. Wrong begin end separator, key: ${key}, begin: \`${s[1]}\`, end: \`${s[2]}\``);
    }
    s = s[1] || s[2];
    resultReg = resultReg.replace(key, `([^${s}]+|${empty})`);
    //console.log('`' + s + '`')
    keys[keyIndex] = key.substr(1);
  });
  resultReg = new RegExp(resultReg);
  //console.log(resultReg)
  return function (msg) {
    let res = {}, t = String(msg).match(resultReg);
    if (!t) {
      return;
    }
    t.forEach((v, i) => {
      if (v === empty) {
        return;
      }
      if (i === 0) {
        return;
      }
      res[keys[i - 1]] = v;
    });
    return res;
  };
}

module.exports.simpleExtractor = simpleExtractor;
module.exports.parseNetworkSyslog = parseNetworkSyslog;
module.exports.parseSyslogData = parseSyslogData;