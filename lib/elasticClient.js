'use strict';

const elasticsearch = require('elasticsearch');
const Agent = require('agentkeepalive');

const PORT = process.env.ELASTIC_PORT || 9200;
const HOST = process.env.ELASTIC_HOST || 'localhost';
const LOGLEVEL = process.env.ELASTIC_LOGLEVEL || 'trace';

const client = elasticsearch.Client({
  host:           `${HOST}:${PORT}`,
  requestTimeout: 60000,
  maxSockets:     5,
  minSockets:     1,
  createNodeAgent(connection, config) {
    return new Agent(connection.makeAgentConfig(config));
  },
  apiVersion:     '5.0',
  log:            LOGLEVEL
});

module.exports.client = client;
