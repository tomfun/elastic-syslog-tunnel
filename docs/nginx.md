### First you need 
Add log format to nginx configuration **/etc/nginx/nginx.conf**:  
```
#.....
http {
#....
    log_format formonitoring '$remote_addr  $remote_user $msec $request_method "$request_filename" "$server_protocol" $status $body_bytes_sent "$http_referer" "$http_user_agent" "$server_name" $request_time $connection $connection_requests "$request_uri" "$args" "$uri"';

    #include /etc/nginx/conf.d/*.conf;
    #include /etc/nginx/sites-enabled/*;
#...
}
```

Format must be exactly the same with ```nginxAccessFormat``` at [lib/chain.js:12](/lib/chain.js#L12)
### Second
Add log destination to syslog in your virtual host (maybe */etc/nginx/sites-enabled/default.conf*):
```
# THIS strings
#error_log /var/log/nginx/siteName_error.log crit;
#access_log /var/log/nginx/siteName_access.log combined;
# REPLACE by this
    error_log syslog:server=127.0.0.1,tag=siteName crit; #just shorter
    access_log syslog:server=127.0.0.1,tag=siteName,severity=info,facility=local7 formonitoring; #just longer
```

**And** don't forget about to change **siteName** with your, without any strange characters.

### Restart it
```
nginx -t && service nginx reload
```