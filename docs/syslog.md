### Install it:  
```
apt-get install syslog-ng syslog-ng-core
```
[google says](https://www.loggly.com/docs/install-syslog-ng/)  

### Copy keys to secure directory, check, this must be available just for root
**mc** or **[scp](http://www.hypexr.org/linux_scp_help.php)** may helps you

### Configure it
Copy or follow instructions in [syslog-ng.conf](/docs/syslog-ng.conf)

### Restart it
```
service syslog-ng restart
```