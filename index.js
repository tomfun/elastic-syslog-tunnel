#!/usr/bin/env node
'use strict';

const fs = require('fs');
const PORT = process.env.INCOME_PORT || 1337;
const HOST = process.env.INCOME_IP || '0.0.0.0';
const TLS_KEY_FILE = process.env.INCOME_TLS_KEY_FILE || './server.key';
const TLS_KEY_PASSPHRASE = process.env.INCOME_TLS_KEY_PASSPHRASE || 'server';
const TLS_CERTIFICATE = process.env.INCOME_TLS_CERTIFICATE || './server.crt';
const TLS_CA = process.env.INCOME_TLS_CA || './ca.crt';

const tls = require('tls');

const chain = require('./lib/chain');

const tlsOptions = {
  requestCert:        true,// клиент обязан предоставить сертификат
  rejectUnauthorized: true, // и сертификат должен быть правельный
  key:                fs.readFileSync(TLS_KEY_FILE),//мы свои, и ключ у нас есть
  passphrase:         TLS_KEY_PASSPHRASE,//и пасфраза
  cert:               fs.readFileSync(TLS_CERTIFICATE),//и сертификат, который не жалко показывать
  ca:                 fs.readFileSync(TLS_CA)//и доверяем мы именно нашему ЦА, которым подписаны клиенты
};

let immediateShutdown = false;

let server = tls.createServer(tlsOptions, function (socket) {
  console.info('new connect');
  const subject = socket.getPeerCertificate().subject || {};
  const address = socket.address() || {};
  console.info(subject.CN, subject.O, address.address);
  socket.on('data', function safeWrapper(data) {
    try {
      chain(data, subject, address, socket);
    } catch (e) {
      console.error(`${e}\n${e ? e.stack : ''}\nclient info:\n    ${subject.CN} ${address.address}`);
      socket.destroy();
    }
    if (immediateShutdown) {
      socket.destroy();
    }
  });
  //socket.write('Echo server\r\n');
  //socket.pipe(socket);
});

console.log('starting on', HOST, PORT);
server.listen(PORT, HOST);

/* Correct shutdown */
process.on('SIGINT', function() {
  immediateShutdown = true;
  server.close(() => {
    require('./lib/elastic')
      .getIterationPromise()
      .then(function() {
      console.log('now we did stuff');
      process.exit(0);
    });
    console.log('socket closed');
  });

  setTimeout(function() {
    process.exit(1);
  }, 10000).ref();
});