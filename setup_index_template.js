#!/usr/bin/env node
'use strict';

const _ = require('lodash');
const path = require('path');
const moment = require('moment');

// start voodoo

const pm2ConfigPath = process.argv[2];
if (!pm2ConfigPath) {
  console.error('specify pm2 config path as first program argument');
  process.exit(1);
}
const pm2Config = require(path.resolve(pm2ConfigPath));
_.defaults(process.env, pm2Config.apps[0].env);
console.log('use elastic configs:\n', _.pickBy(process.env, (v, k) => k.indexOf('ELASTIC') !== -1));

// end voodoo

const client = require('./lib/elasticClient').client;
const mappings = require('./lib/mappings.json');

const templateId = 'syslog-ng-indexes';

client.ping()
  .then(() => client.indices.existsTemplate({name: templateId}))
  .then((exist) => exist ? console.log('changing existing') : console.log('creating'))
  .then(() => client.indices.putTemplate({name: templateId, body: {
    template: 'log-*', // log-brander-2017.01.01
    // template: 'log-*-????.??.??', // log-brander-2017.01.01
    mappings: mappings,
    version: 1,
  }}))
  .then(() => console.log('created successfully'))
  .catch((e) => {
    console.error(e);
    process.exit(1);
  });
